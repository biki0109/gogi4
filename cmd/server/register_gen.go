// IM AUTO GENERATED, BUT CAN BE OVERRIDDEN

package main

import (
	"git.begroup.team/platform-transport/gogi4/config"
	"git.begroup.team/platform-transport/gogi4/internal/services"
	"git.begroup.team/platform-transport/gogi4/internal/stores"
	"git.begroup.team/platform-transport/gogi4/pb"
)

func registerService(cfg *config.Config) pb.Gogi4Server {

	mainStore := stores.NewMainStore()

	return services.New(cfg, mainStore)
}
