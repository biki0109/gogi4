package client

import (
	"context"

	"git.begroup.team/platform-core/kitchen/l"
	"git.begroup.team/platform-transport/gogi4/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
)

type Gogi4Client struct {
	pb.Gogi4Client
}

func NewGogi4Client(address string) *Gogi4Client {
	conn, err := grpc.DialContext(context.Background(), address,
		grpc.WithInsecure(),
		grpc.WithBalancerName(roundrobin.Name),
	)

	if err != nil {
		ll.Fatal("Failed to dial Gogi4 service", l.Error(err))
	}

	c := pb.NewGogi4Client(conn)

	return &Gogi4Client{c}
}
