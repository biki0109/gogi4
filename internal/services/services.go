package services

import (
	"context"

	"git.begroup.team/platform-transport/gogi4/config"
	"git.begroup.team/platform-transport/gogi4/internal/stores"
	"git.begroup.team/platform-transport/gogi4/pb"
)

const Version = "1.0.0"

type service struct {
	isReady   bool
	cfg       *config.Config
	mainStore *stores.MainStore
}

func New(config *config.Config,
	mainStore *stores.MainStore) pb.Gogi4Server {
	return &service{
		isReady:   true,
		cfg:       config,
		mainStore: mainStore,
	}
}

func (s *service) Version(context context.Context, req *pb.VersionRequest) (*pb.VersionResponse, error) {
	return &pb.VersionResponse{
		Version: Version,
	}, nil
}
