module git.begroup.team/platform-transport/gogi4

go 1.13

require (
	contrib.go.opencensus.io/exporter/stackdriver v0.12.8
	git.begroup.team/platform-core/be-central-proto v1.0.0
	git.begroup.team/platform-core/kitchen v1.0.5
	github.com/gogo/googleapis v1.3.0
	github.com/gogo/protobuf v1.3.1
	github.com/golang/protobuf v1.3.2
	github.com/grpc-ecosystem/go-grpc-middleware v1.1.0
	github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0
	github.com/grpc-ecosystem/grpc-gateway v1.11.3
	github.com/k0kubun/pp v3.0.1+incompatible // indirect
	github.com/lib/pq v1.3.0
	github.com/mwitkow/go-proto-validators v0.2.0
	github.com/prometheus/client_golang v1.2.1
	github.com/spf13/viper v1.4.0
	go.opencensus.io v0.22.1
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897 // indirect
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/grpc v1.24.0
	gorm.io/driver/mysql v1.0.2 // indirect
	gorm.io/driver/postgres v1.0.2 // indirect
	gorm.io/gorm v1.20.2
)
